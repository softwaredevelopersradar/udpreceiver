﻿using System.Net;
using System.Windows;

namespace ClientUDP
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private UDPReceiver.UDPReceiver client = new UDPReceiver.UDPReceiver(IPAddress.Parse("127.0.0.1"), 15000, IPAddress.Parse("127.0.0.1"), 15001, 0, 0);

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            client.OnConnect += Client_OnConnect;
            client.OnDisconnect += Client_OnDisconnect;
            client.OnErrorReceiver += Client_OnErrorReceiver;
            client.OnGetCoordAero += Client_OnGetCoord;
            client.OnGetCoordRDM += Client_OnGetCoord;
        }

        private void Client_OnGetCoord(object sender, UDPReceiver.Coord e)
        {
            Dispatcher.Invoke(() => Display.AppendText($"Coord: \r\n Point: Latitude: {e.latitude}\r\n Longitude: {e.longitude}\r\n Altitude: {e.altitude}\r\n"));
        }

        private void Client_OnErrorReceiver(object sender, bool e)
        {
            Dispatcher.Invoke(() => Display.AppendText($"Error Receive Data: {e}\r\n"));
        }


        private void Client_OnDisconnect(object sender, bool e)
        {
            Dispatcher.Invoke(() => Display.AppendText($"Disconnect: {e}\r\n"));
        }

        private void Client_OnConnect(object sender, bool e)
        {
            Dispatcher.Invoke(() => Display.AppendText($"Connect: {e}\r\n"));
        }

        private void ConnectButton_Click(object sender, RoutedEventArgs e)
        {
            client.Connect();
        }

        private void DisconnectButton_Click(object sender, RoutedEventArgs e)
        {
            client.Disconnect();
        }
    }
}
