﻿using System;
using System.Net;
using System.Windows;

namespace ServerUDP
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private UDPReceiver.UDPReceiver server = new UDPReceiver.UDPReceiver(IPAddress.Parse("127.0.0.1"), 15001, IPAddress.Parse("127.0.0.1"), 15000, 0, 0);


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            server.OnConnect += Server_OnConnect;
            server.OnDisconnect += Server_OnDisconnect;
            server.OnSend += Server_OnSend;
        }


        private void Server_OnSend(object sender, byte[] e)
        {
            Display.AppendText("Send Data: ");
            foreach (var i in e)
            {
                Display.AppendText(i.ToString());
            }
            Display.AppendText("\r\n");
        }


        private void Server_OnDisconnect(object sender, bool e)
        {
            Dispatcher.Invoke(() => Display.AppendText($"Disconnect: {e}\r\n"));
        }

        private void Server_OnConnect(object sender, bool e)
        {
            Dispatcher.Invoke(() => Display.AppendText($"Connect: {e}\r\n"));
        }



        private void SendButton_Click(object sender, RoutedEventArgs e)
        {
            UDPReceiver.Coord coord = new UDPReceiver.Coord(1.2, 2.5, (Single)1.98);
            UDPReceiver.Coord coord1 = new UDPReceiver.Coord(2.2, 2.3, (Single)53.98);
            server.SendRDM(coord);
            server.SendAero(coord1);
        }

        private void DisconnectButton_Click(object sender, RoutedEventArgs e)
        {
            server.Disconnect();
        }

        private void ConnectButton_Click(object sender, RoutedEventArgs e)
        {
            server.Connect();
        }
    }

}
