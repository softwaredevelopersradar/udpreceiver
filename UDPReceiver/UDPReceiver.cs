﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static UDPReceiver.CipherEnum;
using static UDPReceiver.ErrorEnum;

namespace UDPReceiver
{
    public class UDPReceiver
    {
        #region private
        private UdpClient udpClient;
        UdpClient receiver;
        UdpClient close = new UdpClient();
        private IPEndPoint remoteIpEndPoint;
        private IPEndPoint myIpEndPoint;
        private Thread readDataThread;
        private Thread readThread;

        private IPAddress myIp;
        private int myPort;
        private IPAddress remoteIp;
        private int remotePort;
        private byte addrSender;
        private byte addrRecipient;

        internal byte[] receiveBytes = null;

        private int count = 0;
        private byte[] latitudeArray = new byte[8];
        private byte[] longitudeArray = new byte[8];
        private double latitude = 0, longitude = 0;
        private float altitude = 0;

        private byte[] informationField = new byte[9];
        private int codogramCounter = 1;
        #endregion

        #region constructors
        public UDPReceiver()
        {
            string addr = "127.0.0.1";
            IPAddress address = IPAddress.Parse(addr);
            this.myIp = address;
            this.myPort = 15000;
            this.remoteIp = address;
            this.remotePort = 15001;
            this.addrSender = 0;
            this.addrRecipient = 0;
        }
        

        public UDPReceiver(IPAddress myIp, int myPort, IPAddress remoteIp, int remotePort, byte addrSender, byte addrRecipient)
        {
            this.myIp = myIp;
            this.myPort = myPort;
            this.remoteIp = remoteIp;
            this.remotePort = remotePort;
            this.addrSender = addrSender;
            this.addrRecipient = addrRecipient;
        }
        #endregion

        #region connect-disconnect

        public void Connect()
        {
            try
            {
                if (udpClient == null)
                {
                    OnReceive += ReceiveData;
                    myIpEndPoint = new IPEndPoint(myIp, myPort);
                    remoteIpEndPoint = new IPEndPoint(remoteIp, remotePort);
                    udpClient = new UdpClient();
                    udpClient.Connect(remoteIpEndPoint);
                    OnConnect?.Invoke(this, true);
                    if (readDataThread == null)
                    {
                        readDataThread = new Thread(Receiver);
                        readDataThread.IsBackground = true;
                        readDataThread.Start();
                    }
                }
            }
            catch
            {
                OnConnect?.Invoke(this, false);
            }
        }

        public void Disconnect()
        {
            try
            {
                receiver.Close();
                OnReceive -= ReceiveData;
                if (readDataThread != null)
                {
                    readDataThread.Abort();
                    readDataThread.Join(500);
                    readDataThread = null;
                }
                if (readThread != null)
                {
                    readThread.Abort();
                    readThread.Join(500);
                    readThread = null;
                }
                if (udpClient != null)
                {
                    close.Close();
                    OnDisconnect?.Invoke(this, true);
                    udpClient = null;
                }
            }
            catch
            {
                OnDisconnect?.Invoke(this, false);
            }
        }
        #endregion

        #region Receive-Send
        private void Receiver()
        {
            receiver = new UdpClient(myPort);
            IPEndPoint remoteIp = null;
            try
            {
                while (true)
                {
                    receiveBytes = receiver.Receive(ref remoteIp);
                    OnReceive?.Invoke(this, receiveBytes);
                }
            }
            catch
            {
                OnErrorReceiver?.Invoke(this, true);
            }
            finally
            {
                receiver.Close();
            }
        }

        public void SendRDM(Coord coord)
        {
            Send(coord, CodogramCiphers.IRI_RDM);
        }

        public void SendAero(Coord coord)
        {
            Send(coord, CodogramCiphers.AeroScope);
        }

        private void Send(Coord coord , CodogramCiphers ciphers)
        {
            byte[] bytes = new byte[53];

            byte[] latitudeArray = new byte[8];
            byte[] longitudeArray = new byte[8];
            byte[] altitudeArray = new byte[4];

            byte[] tau1Array = new byte[8];
            byte[] tau2Array = new byte[8];
            byte[] tau3Array = new byte[8];

            UdpClient sender = new UdpClient();
            try
            {
                latitudeArray = BitConverter.GetBytes(coord.latitude);
                longitudeArray = BitConverter.GetBytes(coord.longitude);
                altitudeArray = BitConverter.GetBytes(coord.altitude);

                CreateInformationField(ciphers);

                informationField.CopyTo(bytes, 0);
                latitudeArray.CopyTo(bytes, 9);
                longitudeArray.CopyTo(bytes, 17);
                altitudeArray.CopyTo(bytes, 25);

                tau1Array = BitConverter.GetBytes(coord.tau1);
                tau2Array = BitConverter.GetBytes(coord.tau2);
                tau3Array = BitConverter.GetBytes(coord.tau3);

                tau1Array.CopyTo(bytes, 29);
                tau2Array.CopyTo(bytes, 37);
                tau3Array.CopyTo(bytes, 45);

                sender.Send(bytes, bytes.Length, remoteIp.ToString(), remotePort);
                OnSend?.Invoke(this, bytes);
            }
            catch
            {
                OnErrorSend?.Invoke(this, true);
            }
            finally
            {
                sender.Close();
            }
        }


        private void CreateInformationField(CodogramCiphers cipher)
        {
            byte[] firstPartField = {addrSender, addrRecipient, (byte)cipher, (byte)codogramCounter};
            byte[] secondPartField = new byte[4];
            secondPartField = BitConverter.GetBytes(28);
            byte[] thirdPartArray = firstPartField.Concat(secondPartField).ToArray();
            thirdPartArray.CopyTo(informationField, 0);
            informationField[8] = 0;
            codogramCounter++;
        }
        #endregion
        
        #region decryption
        private void ReceiveData(object sender, byte[] e)
        {
            try
            {
                if (receiveBytes.Length == 53 )
                {

                    //Получение координат
                    //широта
                    count = 0;
                    for (int i = 9; i < 17; i++)
                    {
                        latitudeArray[count] = receiveBytes[i];
                        count++;
                    }
                    latitude = BitConverter.ToDouble(latitudeArray, 0);

                    //Долгота
                    count = 0;
                    for (int i = 17; i < 25; i++)
                    {
                        longitudeArray[count] = receiveBytes[i];
                        count++;
                    }
                    longitude = BitConverter.ToDouble(longitudeArray, 0);

                    //Высота
                    altitude = BitConverter.ToSingle(receiveBytes, 25);

                    double tau1 = BitConverter.ToDouble(receiveBytes, 29);
                    double tau2 = BitConverter.ToDouble(receiveBytes, 37);
                    double tau3 = BitConverter.ToDouble(receiveBytes, 45);

                    //Ири
                    if (receiveBytes[2] == 1)
                    {
                        OnGetCoordRDM?.Invoke(this, new Coord(latitude, longitude, altitude, tau1, tau2, tau3));
                    }
                    //Аэроскоп
                    else if (receiveBytes[2] == 2)
                    {
                        OnGetCoordAero?.Invoke(this, new Coord(latitude, longitude, altitude, tau1, tau2, tau3));
                    }
                }
            }
            catch { OnErrorDecryption?.Invoke(this, true); }
        }

        
        #endregion

        #region Events
        public event EventHandler<bool> OnConnect;
        public event EventHandler<bool> OnDisconnect;
        public event EventHandler<byte[]> OnReceive;
        public event EventHandler<byte[]> OnSend;
        public event EventHandler<Coord> OnGetCoordRDM;
        public event EventHandler<Coord> OnGetCoordAero;
        //Ивенты для блоков catch
        public event EventHandler<bool> OnErrorDecryption;
        public event EventHandler<bool> OnErrorSend;
        public event EventHandler<bool> OnErrorReceiver;
        #endregion
    }
}
