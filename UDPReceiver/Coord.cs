﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDPReceiver
{
    public class Coord
    {
        public double latitude { get; set; }
        public double longitude { get; set; }
        public float altitude { get; set; }

        public double tau1 { get; set; }
        public double tau2 { get; set; }
        public double tau3 { get; set; }

        public Coord()
        {
            latitude = 0;
            longitude = 0;
            altitude = 0;
            tau1 = 0;
            tau2 = 0;
            tau3 = 0;
        }

        public Coord(double latitude, double longitude, float altitude)
        {
            this.latitude = latitude;
            this.longitude = longitude;
            this.altitude = altitude;
            this.tau1 = 0;
            this.tau2 = 0;
            this.tau3 = 0;
        }

        public Coord(double latitude, double longitude, float altitude, double tau1, double tau2, double tau3)
        {
            this.latitude = latitude;
            this.longitude = longitude;
            this.altitude = altitude;
            this.tau1 = tau1;
            this.tau2 = tau2;
            this.tau3 = tau3;
        }
    }
}
